API -  Application Programming Interface

NodeJs App
Routes
Controller
Error Handling
Database Configuration
Mysql - Data Persistence
Handle HTTP Requests
Handle CORS
SQL Queries 
Docker - Deployment


1st stage 
1 => create a repository : mkdir App ... cd App
2 => npm init -y // creation of the package.json
3 => sudo apt install node-typescript
4 => tsc --init // creation of the tsconfig.json
5 => in tsconfig.json | change target to target:es6,
6 => uncomment "outDir" and add "outDir": "./dist" && same with "rootDir": "./src",
7 => modify the package.json
8 => npm i cors dotenv express ip mysql2 //install node_modules
9 =>npm i -D @types/cors @types/express @types/ip types/mysql2 nodemon ts-node typescript  
10 => create the index.ts in /src
11 => npm run start:build //the dist is created
12 => copy all the files 
13 => npm run start:dev
 
// update nodejs modules :  
npm i npm-check@latest -g

// installing n package
npm install -g n

// update nodejs
n latest

//install mysql server 
wget https://dev.mysql.com/get/mysql-apt-config_0.8.12-1_all.deb

// supression of the previous mysql server 
sudo apt remove mysql mysql-server

// install Mysql package
sudo apt-get install mysql-server

apt install wine 

// install dbForge

link mysql + bench
 https://dev.to/gsudarshan/how-to-install-mysql-and-workbench-on-ubuntu-20-04-localhost-5828

 installation de mysql server:
 sudo apt-cache search -n mysql-server 
 sudo apt-cache search -n mysql-client

sudo apt install mariadb-client-core-10.6

sudo wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2_amd64.deb
sudo dpkg -i libssl1.1_1.1.1f-1ubuntu2_amd64.deb

sudo apt install mysql-community-client-plugins
sudo apt install mysql-community-client-core
sudo apt install mysql-server mysql-client

//verifier l'etat de mysql : /etc/init.d/mysql status

//acces a mysql : sudo mysql -u root -p
 show databases;
 use mysl;
 show tables;
 select Password from user;

 //historique de nos commandes : vim .mysql_history

 lorsque l'installation d'un paquet derrange :  sudo apt --fix-broken install
 sudo apt install dpkg 


/*MYSQL*/

Connexion à mysql : sudo mysql -u root -p 

//telechargement du plugin Mysql pour gerer mes bdd

create database patientsdb;

create user patientsdb@localhost identified by '7391';

grant all privileges on patientsdb.* to patientsdb@localhost;

flush privileges;

 

//test pas bon
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';

To start MySQL server:   /etc/init.d/mysql  start.
To stop MySQL server:    /etc/init.d/mysql  stop
To restart MySQL server: /etc/init.d/mysql  restart

SELECT user,authentication_string,plugin,host FROM mysql.user;

update mysql.user set password=password('nouveaumotdepasse') where user='root'; 
update mysql.user set authentication_string=password('root') where user='root'; 
FLUSH PRIVILEGES;
 
//pour se connecter à mysql : mysql -u patientsdb -p

// affichage des tables : describe patients;
// affichage des elements d'une table select * from patients;

// sudo nano /etc/mysql/my.cnf

//taper 
[mysqld]
skip-grant-tables
skip-networking

// arreter le service : /etc/init.d/mysql stop
// lancer le service : /etc/init.d/mysql start

UPDATE mysql.user SET password=password('') WHERE user='root' AND host='localhost';

//connexion root  : mysql -uroot -p 
// mdp : root

changer le mdp : ALTER USER 'root'@'localhost' IDENTIFIED BY '';

//insertion dbb
INSERT INTO patients(firstname, last_name, email, address, diagnosis, phone, status, image_url) VALUES ('ryuk','ran','ran@gmail.com','Quelquepart,1200perdu','Negatif','0646565','statusInconnu','img1.jpg');

select * from patients;

// lancer mysql : systemctl start  mysql.service
//statut  systemctl status mysql.service

//pour reinitialiser mysql :
sudo apt autoremove --purge mysql\* mariadb\* ou sudo apt autoremove --purge mysql\* 
sudo apt clean
sudo mv /var/lib/mysql /var/lib/mysql_bak
sudo mv /etc/mysql /etc/mysql_bak
sudo apt install mysql-server

//se connecter : sudo mysql -u root

creation de la bd : create database patientsdb;

associer les privileges : grant all privileges on patientsdb.* to 'root'@'localhost';

flush privileges;

creation des tables: 
CREATE TABLE patients
( 
    id          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    firstname   VARCHAR(255) DEFAULT NULL,
    last_name   VARCHAR(255) DEFAULT NULL,
    email       VARCHAR(255) DEFAULT NULL,
    address     VARCHAR(255) DEFAULT NULL, 
    diagnosis   VARCHAR(255) DEFAULT NULL,
    phone       VARCHAR(30) DEFAULT NULL,
    status      VARCHAR(30) DEFAULT NULL,
    created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    image_url   VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id),
    CONSTRAINT  UQ_Patients_Email UNIQUE (email)
) AUTO_INCREMENT = 1;

insertion :
 INSERT INTO patients(firstname, last_name, email, address, diagnosis, phone, status, image_url) VALUES ('ryuk','ran','ran@gmail.com','Quelquepart,1200perdu','Negatif','0646565','statusInconnu','img1.jpg');


select USER(),CURRENT_USER();
show grants for 'root'@'localhost';

//change mdp root
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '';


//Frontend
installer angular : npm install -g @angular/cli
ajout d'un service : ng g service service/patient
 
//lance server : npm run start:dev